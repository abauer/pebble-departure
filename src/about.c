/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>

#include "departure.h"

static Window *window;

static TextLayer *text;

static void
window_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  GRect rect =
    { .origin =
      { 0, 0 }, .size =
      { bounds.size.w, bounds.size.h } };

  text = text_layer_create(rect);
  text_layer_set_text(text, "Departure Monitor\nVersion 2.0\n\n (C) Andreas Bauer 2016");
  text_layer_set_text_alignment(text, GTextAlignmentLeft);
  text_layer_set_font(text, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_overflow_mode(text,GTextOverflowModeWordWrap);
  layer_add_child(window_layer, text_layer_get_layer(text));


}

static void
window_appear(Window *window)
{


}

static void
window_unload(Window *window)
{


}

void
about_show_window()
{
  window_stack_push(window, true);

}

void
about_init(void)
{
  window = window_create();
  window_set_window_handlers(window, (WindowHandlers
        )
          { .load = window_load, .unload = window_unload, .appear = window_appear });
}

void
about_deinit(void)
{
  window_destroy(window);
}



