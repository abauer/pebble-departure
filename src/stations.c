/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>

#include <departure.h>

static Window *window;

int8_t StationsState = 0;

time_t LastUpdate = 0;

void
stations_reset(int8_t mode)
{
  if(mode == 1)
  {
    if(CurrentView == VIEW_STATIONS1 || CurrentView == VIEW_STATIONS2 || CurrentView == VIEW_STATIONS3 || CurrentView == VIEW_STATIONS4)
    {
      window_stack_pop(window);
    }
    
    StationsState = STATE_NONE;
    LastUpdate = 0;
    NumSavedStations = 0;
    NumNearbyStations = 0;
  }
}

char *
itoa(int num, char* buff)
{
  int i = 0, temp_num = num, length = 0;
  char *string = buff;

  if (num >= 0)
    {
      // count how many characters in the number
      while (temp_num)
        {
          temp_num /= 10;
          length++;
        }

      // assign the number to the buffer starting at the end of the
      // number and going to the begining since we are doing the
      // integer to character conversion on the last number in the
      // sequence
      for (i = 0; i < length; i++)
        {
          buff[(length - 1) - i] = '0' + (num % 10);
          num /= 10;
        }
      buff[i] = '\0'; // can't forget the null byte to properly end our string
    }
  else
    return "Unsupported Number";

  return string;
}

static MenuLayer *menu_layer;

void
stations_message_handler(DictionaryIterator *iter, void *context)
{
  Tuple *stations = dict_find(iter, KEY_STATIONS);

  if (stations)
    {
      if (stations->value->int32 == 1)
        {
          Tuple *provider = dict_find(iter, KEY_PROVIDER);
          if(provider->value->int32 != Provider)
            {
              return;
            }
          Tuple *numitems = dict_find(iter, KEY_NUMITEMS);

          int num = numitems->value->int32;

          if (num > MAX_STATIONS)
            num = MAX_STATIONS;

          uint8_t name_keys[num];
          uint8_t name2_keys[num];
          uint8_t id_keys[num];

          Tuple* keys = dict_find(iter, KEY_STATION_NAME);
          memcpy(name_keys, keys->value->data, num);

          keys = dict_find(iter, KEY_STATION_ID);
          memcpy(id_keys, keys->value->data, num);

          keys = dict_find(iter, KEY_STATION_NAME2);
          memcpy(name2_keys, keys->value->data, num);

          int i;
          for (i = 0; i < num; i++)
            {
              Tuple *stationID = dict_find(iter, id_keys[i]);
              Tuple *stationName = dict_find(iter, name_keys[i]);
              Tuple *stationName2 = dict_find(iter, name2_keys[i]);
              strncpy(NearbyStations[i].name, stationName->value->cstring,
              NAME_LENGTH);
              strncpy(NearbyStations[i].name2, stationName2->value->cstring,
              NAME_LENGTH);
              NearbyStations[i].id = stationID->value->int32;
            }

          StationsState = STATE_READY;
          NumNearbyStations = num;

          LastUpdate = time(NULL);

          menu_layer_reload_data(menu_layer);
          layer_mark_dirty(menu_layer_get_layer(menu_layer));
        }
      else if (stations->value->int32 < 0)
        {
          StationsState = stations->value->int32;
          menu_layer_reload_data(menu_layer);
          layer_mark_dirty(menu_layer_get_layer(menu_layer));
          APP_LOG(APP_LOG_LEVEL_DEBUG, "Error %d", (int ) stations->value->int32);
        }
    }

}

void send_stations_request()
{
  if(JSReady == 0)
    {
      return;
    }

  if(time(NULL) - LastUpdate < REFRESH_INTERVAL)
    {
      return;
    }

  Tuplet stops = TupletInteger(KEY_STATIONS, 1);
  Tuplet provider = TupletInteger(KEY_PROVIDER, Provider);

  DictionaryIterator *iter;
  app_message_outbox_begin(&iter);

  if (iter == NULL)
    {
      return;
    }

  dict_write_tuplet(iter, &stops);
  dict_write_tuplet(iter, &provider);
  dict_write_end(iter);

  app_message_outbox_send();

  if(StationsState != STATE_READY)
    StationsState = STATE_LOADING;
}

static void
menu_select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data)
{

  station* station = 0;
  if (cell_index->section == 1)
    {
      if (NumNearbyStations > 0 && StationsState == STATE_READY)
        {
          station = &NearbyStations[cell_index->row];

        }
      else
        {
          station = 0;
        }
    }
  else if (cell_index->section == 2)
    {
      if (NumSavedStations > 0)
        {
          station = &SavedStations[cell_index->row];
        }
    }

  if(station != 0)
    {
      if(CurrentView == VIEW_STATIONS1)
        {
          departure_set_station(station);
          window_stack_pop(false);
          departure_show_window();
        }
      else if(CurrentView == VIEW_STATIONS2)
        {
          trip_set_station_start(station);
          window_stack_pop(false);
          stations_show_window(VIEW_STATIONS3);
        }
      else if(CurrentView == VIEW_STATIONS3)
        {
          trip_set_station_stop(station);
          window_stack_pop(false);
          trips_show_window();
        }
      else if(CurrentView == VIEW_STATIONS4)
        {
          trip_set_station_start(station);
          window_stack_pop(false);
        }
      else if(CurrentView == VIEW_STATIONS5)
        {
          trip_set_station_stop(station);
          window_stack_pop(false);
        }
    }
}

// A callback is used to specify the amount of sections of menu items
// With this, you can dynamically add and remove sections
static uint16_t
menu_get_num_sections_callback(MenuLayer *menu_layer, void *data)
{
  return 3;
}

// Each section has a number of items;  we use a callback to specify this
// You can also dynamically add and remove items using this
static uint16_t
menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data)
{
  switch (section_index)
    {
  case 0:
    return 0;
    break;
  case 1:
    if ((StationsState == STATE_LOADING) || StationsState < 0 || (StationsState == STATE_READY && NumNearbyStations == 0) || (JSReady==0) )
      return 1;
    else
      return NumNearbyStations;
    break;
  case 2:
    if (NumSavedStations == 0)
      return 1;
    else
      return NumSavedStations;
    break;
  default:
    return 0;
    }
}

// A callback is used to specify the height of the section header
static int16_t
menu_get_header_height_callback(MenuLayer *menu_layer, uint16_t section_index, void *data)
{
  // This is a define provided in pebble.h that you may use for the default height
  return MENU_CELL_BASIC_HEADER_HEIGHT;
}

// Here we draw what each header is
static void
menu_draw_header_callback(GContext* ctx, const Layer *cell_layer, uint16_t section_index, void *data)
{
  // Determine which section we're working with
  switch (section_index)
    {
  case 0:
    if(CurrentView == VIEW_STATIONS1)
      {
        menu_cell_basic_header_draw(ctx, cell_layer, "Station");
      }
    else if(CurrentView == VIEW_STATIONS2 || CurrentView == VIEW_STATIONS4)
      {
        menu_cell_basic_header_draw(ctx, cell_layer, "From");
      }
    else if(CurrentView == VIEW_STATIONS3 || CurrentView == VIEW_STATIONS5)
      {
        menu_cell_basic_header_draw(ctx, cell_layer, "To");
      }
    break;
  case 1:
    menu_cell_basic_header_draw(ctx, cell_layer, "Nearby stations");
    break;
  case 2:
    menu_cell_basic_header_draw(ctx, cell_layer, "Saved stations");
    break;
    }
}

// This is the menu item draw callback where you specify what each item should look like
static void
menu_draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data)
{
  // Determine which section we're going to draw in
  switch (cell_index->section)
    {
  case 1:
    if(JSReady == 0)
      {
        menu_cell_basic_draw(ctx, cell_layer, "connecting...", "", NULL);
        return;
      }
    if (StationsState == STATE_LOADING)
      menu_cell_basic_draw(ctx, cell_layer, "loading...", "", NULL);
    else if (StationsState < 0)
      menu_cell_basic_draw(ctx, cell_layer, "error", "", NULL);
    else if (StationsState == STATE_READY && NumNearbyStations == 0)
      menu_cell_basic_draw(ctx, cell_layer, "none found", "", NULL);
    else
      menu_cell_basic_draw(ctx, cell_layer, NearbyStations[cell_index->row].name, NearbyStations[cell_index->row].name2, NULL);
    break;
  case 2:
    if (NumSavedStations == 0)
      menu_cell_basic_draw(ctx, cell_layer, "long press SELECT", "to save a station", NULL);
    else
      menu_cell_basic_draw(ctx, cell_layer, SavedStations[cell_index->row].name, SavedStations[cell_index->row].name2, NULL);
    break;
    }

}

static void
window_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);
  // Create the menu layer
  menu_layer = menu_layer_create(bounds);

  // Set all the callbacks for the menu layer
  menu_layer_set_callbacks(menu_layer, NULL, (MenuLayerCallbacks
        )
          { .get_num_sections = menu_get_num_sections_callback, .get_num_rows = menu_get_num_rows_callback, .get_header_height =
              menu_get_header_height_callback, .draw_header = menu_draw_header_callback, .draw_row = menu_draw_row_callback, .select_click =
              menu_select_callback, });

  // Bind the menu layer's click config provider to the window for interactivity
  menu_layer_set_click_config_onto_window(menu_layer, window);

  // Add it to the window for display
  layer_add_child(window_layer, menu_layer_get_layer(menu_layer));

}

static void
window_appear(Window *window)
{
  
  send_stations_request();
  menu_layer_reload_data(menu_layer);
  menu_layer_set_selected_index(menu_layer, (MenuIndex
        )
          { .section = 0, .row = 0 }, MenuRowAlignBottom, false);
  layer_mark_dirty(menu_layer_get_layer(menu_layer));
}

static void
window_unload(Window *window)
{
  menu_layer_destroy(menu_layer);

}

void
stations_show_window(int8_t view)
{
  window_stack_push(window, true);
  CurrentView = view;

}

void
stations_init(void)
{
  StationsState = STATE_LOADING;
  
  window = window_create();
  window_set_window_handlers(window, (WindowHandlers
        )
          { .load = window_load, .unload = window_unload, .appear = window_appear });
}

void
stations_deinit(void)
{
  window_destroy(window);
}
