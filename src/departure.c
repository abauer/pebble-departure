/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>

#include <departure.h>

static Window *window;
static ScrollLayer *scroll_layer;

static TextLayer *text_message;
static TextLayer *text_station;
static TextLayer *text_line[MAX_DEPARTURES];
static TextLayer *text_time[MAX_DEPARTURES];
static TextLayer *text_dest[MAX_DEPARTURES];

//static InverterLayer *inverter_layer;

time_t LastUpdateDepartures = 0;

int8_t DepartureState = STATE_NONE;

int8_t ViewMode = 0;

station CurrentStation;


departure Departures[MAX_DEPARTURES];
int8_t NumDepartures;

void
departure_reset(int8_t mode)
{
  if(mode == 1)
  {
    if(CurrentView == VIEW_DEPARTURE)
    {
      window_stack_pop(window);
    }
    
    NumDepartures = 0;
    DepartureState = STATE_NONE;
    LastUpdateDepartures = 0;
  }
  

  
  departure_list_mode();
  departure_update_window();
}

void
departure_set_station(station* new_station)
{
  memcpy(&CurrentStation, new_station, sizeof(station));
  DepartureState = STATE_LOADING;
  NumDepartures = 0;
  LastUpdateDepartures = 0;
}

void
update_time_str(time_t dept, char* buffer)
{
  time_t diff = dept - time(NULL);

  if (diff < 0)
    diff = 0;

  int32_t hours = (diff / 3600);
  int32_t minutes = (diff - hours * 3600) / 60;
  //APP_LOG(APP_LOG_LEVEL_DEBUG, "dept: %d, time %d, Timediff %d",(int) dept, (int) time(NULL),(int) diff);

  //if (minutes == 0 && diff > 30)
  //  minutes = 1; //round up to one minute

  if (minutes == 0 && hours == 0)
    {
      strncpy(buffer, "now", 4);
    }
  else if (hours == 0)
    {
      digits(minutes, buffer, false);
    }
  else
    {
      int length = digits(hours, buffer, false);
      buffer[length] = 'h';
      length++;
      digits(minutes, buffer + length, true);
    }

  //APP_LOG(APP_LOG_LEVEL_DEBUG, buffer, window);
}

static void
show_message(const char* message, const char* heading)
{
  text_layer_set_text(text_station, heading);
  layer_mark_dirty(text_layer_get_layer(text_station));

  text_layer_set_text(text_message, message);

  layer_set_hidden(text_layer_get_layer(text_message), false);
  layer_set_hidden(scroll_layer_get_layer(scroll_layer), true);

  layer_mark_dirty(scroll_layer_get_layer(scroll_layer));
  layer_mark_dirty(text_layer_get_layer(text_message));

}

void
departure_update_window()
{
  if(CurrentView != VIEW_DEPARTURE)
  {
    return;
  }
  
  if(JSReady == 0)
    {
      show_message("connecting to phone", "Departure Monitor");
      return;
    }

  if (DepartureState == STATE_LOADING)
    {
      show_message("loading ...", CurrentStation.name);
    }
  else if (DepartureState == STATE_NONE)
    {
      show_message("Press SELECT to choose a station", "Departure Monitor");
    }

  else if (DepartureState == STATE_READY)
    {
      text_layer_set_text(text_station, CurrentStation.name);
      layer_mark_dirty(text_layer_get_layer(text_station));

      int i;
      for (i = 0; i < NumDepartures; i++)
        {
          text_layer_set_text(text_line[i], Departures[i].line);
          text_layer_set_text(text_dest[i], Departures[i].dest);

          update_time_str(Departures[i].time, Departures[i].time_str);
          text_layer_set_text(text_time[i], Departures[i].time_str);

          if(Departures[i].rt)
            {
              text_layer_set_font(text_time[i], fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
            }
          else
            {
              text_layer_set_font(text_time[i], fonts_get_system_font(FONT_KEY_GOTHIC_18));
            }

          layer_mark_dirty(text_layer_get_layer(text_line[i]));
          layer_mark_dirty(text_layer_get_layer(text_dest[i]));
          layer_mark_dirty(text_layer_get_layer(text_time[i]));
        }
      for (; i < MAX_DEPARTURES; i++)
        {
          text_layer_set_text(text_line[i], "");
          text_layer_set_text(text_dest[i], "");
          text_layer_set_text(text_time[i], "");

          layer_mark_dirty(text_layer_get_layer(text_line[i]));
          layer_mark_dirty(text_layer_get_layer(text_dest[i]));
          layer_mark_dirty(text_layer_get_layer(text_time[i]));
        }

      Layer *window_layer = window_get_root_layer(window);
      GRect bounds = layer_get_bounds(window_layer);
      if(ViewMode == VIEW_LONG)
        {
          scroll_layer_set_content_size(scroll_layer, GSize(bounds.size.w, NumDepartures * 2 * 24));
        }
      else
        {
          scroll_layer_set_content_size(scroll_layer, GSize(bounds.size.w, NumDepartures * 24));
        }

      layer_set_hidden(text_layer_get_layer(text_message), true);
      layer_set_hidden(scroll_layer_get_layer(scroll_layer), false);

      layer_mark_dirty(scroll_layer_get_layer(scroll_layer));
      layer_mark_dirty(text_layer_get_layer(text_message));
    }
  else
    {
      show_message("Error", "Departure Monitor");
    }
}

static void
second_tick(struct tm *tick_time, TimeUnits units_changed)
{

  if (DepartureState > STATE_NONE)
    {
      if (time(NULL) - LastUpdateDepartures > REFRESH_INTERVAL)
        {
          send_departure_request();
        }
      if (DepartureState == STATE_READY)
        {
          departure_update_window();
        }
    }

}

void
send_departure_request()
{
  if(JSReady == 0)
    return;

  Tuplet departure = TupletInteger(KEY_DEPARTURES, 1);
  Tuplet station = TupletInteger(KEY_STATION_ID, CurrentStation.id);
  Tuplet provider = TupletInteger(KEY_PROVIDER, Provider);

  DictionaryIterator *iter;
  app_message_outbox_begin(&iter);
  if (iter == NULL)
    {
      return;
    }

  dict_write_tuplet(iter, &departure);
  dict_write_tuplet(iter, &station);
  dict_write_tuplet(iter, &provider);
  dict_write_end(iter);

  app_message_outbox_send();
  LastUpdateDepartures = time(NULL);
}

void
departure_message_handler(DictionaryIterator *iter, void *context)
{
  Tuple *departure = dict_find(iter, KEY_DEPARTURES);

  if (departure)
    {
      if (departure->value->int32 == 1)
        {
          Tuple *provider = dict_find(iter, KEY_PROVIDER);
          if(provider->value->int32 != Provider)
            {
              return;
            }
          Tuple *station_id = dict_find(iter, KEY_STATION_ID);

          if(station_id->value->int32 != CurrentStation.id)
            {
              return;
            }

          Tuple * numitems = dict_find(iter, KEY_NUMITEMS);
          int num = numitems->value->int32;

          Tuple * lt = dict_find(iter, KEY_LOCALTIME);
          time_t offset = lt->value->int32 - time(NULL);
          APP_LOG(APP_LOG_LEVEL_DEBUG, "offset %d, serveryime %d, time %d", (int ) offset, (int ) lt->value->int32, (int) time(NULL));

          if (num > MAX_DEPARTURES)
            num = MAX_DEPARTURES;

          uint8_t line_keys[num];
          uint8_t time_keys[num];
          uint8_t dest_keys[num];
          uint8_t rt[num];

          Tuple* keys = dict_find(iter, KEY_DEPARTURE_LINE);
          memcpy(line_keys, keys->value->data, num);

          keys = dict_find(iter, KEY_DEPARTURE_TIME);
          memcpy(time_keys, keys->value->data, num);

          keys = dict_find(iter, KEY_DEPARTURE_DEST);
          memcpy(dest_keys, keys->value->data, num);

          keys = dict_find(iter, KEY_DEPARTURE_RT);
          memcpy(rt, keys->value->data, num);

          int i;
          for (i = 0; i < num; i++)
            {
              //APP_LOG(APP_LOG_LEVEL_DEBUG, itoa((int)id_keys[i],buffer));

              Tuple *line = dict_find(iter, line_keys[i]);
              Tuple *time = dict_find(iter, time_keys[i]);
              Tuple *dest = dict_find(iter, dest_keys[i]);

              strncpy(Departures[i].line, line->value->cstring, LINE_LENGTH);
              strncpy(Departures[i].dest, dest->value->cstring, NAME_LENGTH);
              Departures[i].time = time->value->int32 - offset;
              Departures[i].rt = rt[i];
            }

          NumDepartures = num;

          DepartureState = STATE_READY;
        }
      else if (departure->value->int32 < 0)
        {
          APP_LOG(APP_LOG_LEVEL_DEBUG, "Error");
          DepartureState = departure->value->int32;

        }

      if(CurrentView == VIEW_DEPARTURE)
        {
          departure_update_window();
        }

    }

}

static void
select_click_handler(ClickRecognizerRef recognizer, void *context)
{
  ;
}

static void
up_click_handler(ClickRecognizerRef recognizer, void *context)
{
  scroll_layer_scroll_up_click_handler(recognizer, scroll_layer);
}

static void
down_click_handler(ClickRecognizerRef recognizer, void *context)
{
  scroll_layer_scroll_down_click_handler(recognizer, scroll_layer);
}

static void
click_config_provider(void *context)
{
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

static void
window_appear(Window *window)
{
  CurrentView = VIEW_DEPARTURE;

  if (DepartureState > STATE_NONE)
    {
      send_departure_request();
      tick_timer_service_subscribe(SECOND_UNIT, second_tick);
    }

  scroll_layer_set_content_offset(scroll_layer, GPointZero, false);

  departure_list_mode();
  departure_update_window();
}

static void
window_disappear(Window *window)
{
  tick_timer_service_unsubscribe();
}

void
departure_list_mode()
{
  if(CurrentView != VIEW_DEPARTURE)
  {
    return;
  }
  
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  int i;
  for (i = 0; i < MAX_DEPARTURES; i++)
    {

      if (ViewMode == VIEW_LONG)
        {
          text_layer_set_font(text_line[i], fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
          text_layer_set_size(text_line[i], (GSize) { bounds.size.w * 0.70, 22 });
          layer_set_frame(text_layer_get_layer(text_line[i]), (GRect
                )
                  { .origin =
                    { 0, (2*i) * 24 }, .size =
                    { bounds.size.w * 0.70, 22 } });

          text_layer_set_size(text_dest[i], (GSize) { bounds.size.w, 22 });
          layer_set_frame(text_layer_get_layer(text_dest[i]), (GRect
                )
                  { .origin =
                    { 0, (2*i+1) * 24 }, .size =
                    { bounds.size.w, 22 } });

          layer_set_frame(text_layer_get_layer(text_time[i]), (GRect
                )
                  { .origin =
                    { bounds.size.w * 0.75, (2*i) * 24 }, .size =
                    { bounds.size.w * 0.25, 22 } });

        }

      else
        {
          text_layer_set_font(text_line[i], fonts_get_system_font(FONT_KEY_GOTHIC_18));
          text_layer_set_size(text_line[i], (GSize) { bounds.size.w * 0.31, 22 });
          layer_set_frame(text_layer_get_layer(text_line[i]), (GRect
                )
                  { .origin =
                    { 0, (i) * 24 }, .size =
                    { bounds.size.w * 0.31, 22 } });

          text_layer_set_size(text_dest[i], (GSize) { bounds.size.w * 0.43, 22 });
          layer_set_frame(text_layer_get_layer(text_dest[i]), (GRect
                )
                  { .origin =
                    { bounds.size.w * 0.32, (i) * 24 }, .size =
                    { bounds.size.w * 0.43, 22 } });

          layer_set_frame(text_layer_get_layer(text_time[i]), (GRect
                )
                  { .origin =
                    { bounds.size.w * 0.75, (i) * 24 }, .size =
                    { bounds.size.w * 0.25, 22 } });

        }

    }
}

static void
window_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  GRect station_rect =
    { .origin =
      { 0, 0 }, .size =
      { bounds.size.w, 32 } };

  text_station = text_layer_create(station_rect);
  text_layer_set_text(text_station, "");
  text_layer_set_text_alignment(text_station, GTextAlignmentLeft);
  text_layer_set_font(text_station, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
  layer_add_child(window_layer, text_layer_get_layer(text_station));

  //inverter_layer = inverter_layer_create(station_rect);
  //layer_add_child(window_layer, inverter_layer_get_layer(inverter_layer));

  // Initialize the scroll layer
  scroll_layer = scroll_layer_create((GRect
        )
          { .origin =
            { 0, 32 }, .size =
            { bounds.size.w, bounds.size.h - 32 } });

  //scroll_layer_set_click_config_onto_window(scroll_layer, window);
  scroll_layer_set_content_size(scroll_layer, GSize(bounds.size.w, 0));

  int8_t i = 0;
  for (i = 0; i < MAX_DEPARTURES; i++)
    {
      text_line[i] = text_layer_create((GRect
            )
              { .origin =
                { 0, (i) * 24 }, .size =
                { bounds.size.w * 0.31, 22 } });
      text_layer_set_text(text_line[i], "");
      text_layer_set_text_alignment(text_line[i], GTextAlignmentLeft);
      text_layer_set_font(text_line[i], fonts_get_system_font(FONT_KEY_GOTHIC_18));
      scroll_layer_add_child(scroll_layer, text_layer_get_layer(text_line[i]));

      text_dest[i] = text_layer_create((GRect
            )
              { .origin =
                { bounds.size.w * 0.32, (i) * 24 }, .size =
                { bounds.size.w * 0.43, 22 } });
      text_layer_set_text(text_dest[i], "");
      text_layer_set_text_alignment(text_dest[i], GTextAlignmentLeft);
      text_layer_set_font(text_dest[i], fonts_get_system_font(FONT_KEY_GOTHIC_18));
      scroll_layer_add_child(scroll_layer, text_layer_get_layer(text_dest[i]));

      text_time[i] = text_layer_create((GRect
            )
              { .origin =
                { bounds.size.w * 0.75, (i) * 24 }, .size =
                { bounds.size.w * 0.25, 22 } });
      text_layer_set_text(text_time[i], "");
      text_layer_set_text_alignment(text_time[i], GTextAlignmentRight);
      text_layer_set_font(text_time[i], fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
      scroll_layer_add_child(scroll_layer, text_layer_get_layer(text_time[i]));

    }

  layer_set_hidden(scroll_layer_get_layer(scroll_layer), true);
  layer_add_child(window_layer, scroll_layer_get_layer(scroll_layer));

  text_message = text_layer_create((GRect
        )
          { .origin =
            { 0, 32 }, .size =
            { bounds.size.w, bounds.size.h - 32 } });
  text_layer_set_text(text_message, "");
  text_layer_set_text_alignment(text_message, GTextAlignmentLeft);
  text_layer_set_font(text_message, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  layer_set_hidden(text_layer_get_layer(text_message), true);
  layer_add_child(window_layer, text_layer_get_layer(text_message));

}

static void
window_unload(Window *window)
{

  int8_t i = 0;
  for (i = 0; i < MAX_DEPARTURES; i++)
    {
      text_layer_destroy(text_line[i]);
      text_layer_destroy(text_time[i]);
      text_layer_destroy(text_dest[i]);
    }
  //inverter_layer_destroy(inverter_layer);
  text_layer_destroy(text_station);

  text_layer_destroy(text_message);

  scroll_layer_destroy(scroll_layer);

}

void
departure_show_window()
{
  window_stack_push(window, true);
}

void
departure_init()
{
  window = window_create();
  window_set_click_config_provider(window, click_config_provider);
  window_set_window_handlers(window, (WindowHandlers
        )
          { .load = window_load, .unload = window_unload, .appear = window_appear, .disappear = window_disappear, });
}

void
departure_deinit()
{
  window_destroy(window);
}
