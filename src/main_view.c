/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>

#include <departure.h>

static Window *window;

static MenuLayer *menu_layer;

static void
menu_select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data)
{
  int8_t offset = 0;
  if(DepartureState > STATE_NONE)
    {
      offset += 1;
    }

  if(cell_index->section == 0 && DepartureState > STATE_NONE)
    {
      departure_show_window();
    }
  else if(cell_index->section == offset)
      {
          if (cell_index->row == 0)
            {
              stations_show_window(VIEW_STATIONS1);
            }
          else if (cell_index->row ==1 )
            {
              stations_show_window(VIEW_STATIONS2);
            }
    }

  else if(cell_index->section == 1 + offset)
    {
      if (cell_index->row == 0)
        {
          about_show_window();
        }
    }

}

// A callback is used to specify the amount of sections of menu items
// With this, you can dynamically add and remove sections
static uint16_t
menu_get_num_sections_callback(MenuLayer *menu_layer, void *data)
{
  int8_t result = 2;

  if(DepartureState > STATE_NONE)
    {
      result += 1;
    }

  return result;
}

// Each section has a number of items;  we use a callback to specify this
// You can also dynamically add and remove items using this
static uint16_t
menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data)
{

  int8_t offset = 0;
  if(DepartureState > STATE_NONE)
    {
      offset += 1;
    }
    
  if(DepartureState > STATE_NONE && section_index == 0)
  {
    return 1;
  }
  else if(section_index == offset)
  {
    return 2;
  }
  else
  {
    return 1;
  }

  return 0;
}

// A callback is used to specify the height of the section header
static int16_t
menu_get_header_height_callback(MenuLayer *menu_layer, uint16_t section_index, void *data)
{
  // This is a define provided in pebble.h that you may use for the default height
  return MENU_CELL_BASIC_HEADER_HEIGHT;
}

// A callback is used to specify the height of the section header
static int16_t
menu_get_cell_height_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data)
{
  int8_t offset = 0;
  if(DepartureState > STATE_NONE)
    {
      offset += 1;
    }

  // Determine which section we're going to draw in
  if(cell_index->section == 0 && DepartureState > STATE_NONE)
    {
     return 44;
    }
  else if(cell_index->section == offset)
    {
        return 30;
    }
  else if(cell_index->section == offset + 1)
    {
    if (cell_index->row == 0)
      {
       return 30;
      }
    else if (cell_index->row == 1)
      {
       return 44;

      }
    }

  return 44;

}

// Here we draw what each header is
static void
menu_draw_header_callback(GContext* ctx, const Layer *cell_layer, uint16_t section_index, void *data)
{
  int8_t offset = 0;
  if(DepartureState > STATE_NONE)
    {
      offset += 1;
    }

  // Determine which section we're working with
  if(section_index == 0 && DepartureState > STATE_NONE)
    {
      menu_cell_basic_header_draw(ctx, cell_layer, "Departure");
    }
  else if(section_index == offset)
    {
      menu_cell_basic_header_draw(ctx, cell_layer, "Main");
    }
  else if(section_index == 1 + offset)
    {
      menu_cell_basic_header_draw(ctx, cell_layer, "About");
    }
}

// This is the menu item draw callback where you specify what each item should look like
static void
menu_draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data)
{
  int8_t offset = 0;
  if(DepartureState > STATE_NONE)
    {
      offset += 1;
    }

  // Determine which section we're going to draw in
  if(cell_index->section == 0 && DepartureState > STATE_NONE)
    {
      menu_cell_basic_draw(ctx, cell_layer, CurrentStation.name, CurrentStation.name2, NULL);
    }
  else if(cell_index->section == offset)
    {
    switch(cell_index->row)
    {
    case 0:
      menu_cell_basic_draw(ctx, cell_layer, "Departures", NULL, NULL);
    break;

    case 1:
      menu_cell_basic_draw(ctx, cell_layer, "Journey", NULL, NULL);
    break;
    }
    }
  else if(cell_index->section == offset + 1)
    {
    if (cell_index->row == 0)
      {
        menu_cell_basic_draw(ctx, cell_layer, "About", NULL, NULL);
      }
    }

}

static void
window_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);
  // Create the menu layer
  menu_layer = menu_layer_create(bounds);

  // Set all the callbacks for the menu layer
  menu_layer_set_callbacks(menu_layer, NULL, (MenuLayerCallbacks
        )
          { .get_num_sections = menu_get_num_sections_callback, .get_num_rows = menu_get_num_rows_callback, .get_header_height =
              menu_get_header_height_callback, .get_cell_height=menu_get_cell_height_callback ,.draw_header = menu_draw_header_callback, .draw_row = menu_draw_row_callback, .select_click =
              menu_select_callback, });

  // Bind the menu layer's click config provider to the window for interactivity
  menu_layer_set_click_config_onto_window(menu_layer, window);

  // Add it to the window for display
  layer_add_child(window_layer, menu_layer_get_layer(menu_layer));

}

static void
window_appear(Window *window)
{
  CurrentView = VIEW_MAIN;
  menu_layer_reload_data(menu_layer);
  layer_mark_dirty(menu_layer_get_layer(menu_layer));
}

static void
window_unload(Window *window)
{
  menu_layer_destroy(menu_layer);

}

void
main_view_show_window()
{
  window_stack_push(window, true);

}

void
main_view_init(void)
{
  window = window_create();
  window_set_window_handlers(window, (WindowHandlers
        )
          { .load = window_load, .unload = window_unload, .appear = window_appear });
}

void
main_view_deinit(void)
{
  window_destroy(window);
}
