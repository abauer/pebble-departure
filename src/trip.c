/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>

#include <departure.h>

int8_t TripState = STATE_NONE;

int8_t TripNum = 0;

int32_t NumStations = 0;
station* Stations = 0;


static Window *window;

typedef struct
{
  time_t start;
    time_t stop;
    int32_t startID;
    int32_t stopID;
    char startPlatform[LINE_LENGTH];
    char stopPlatform[LINE_LENGTH];
    char train[LINE_LENGTH];
    char destination[NAME_LENGTH];
    
} leg;

int32_t NumLegs = 0;
leg* Legs = 0;

void
trip_reset(int8_t mode)
{
  if(mode == 1)
  {
    if(CurrentView == VIEW_TRIP)
    {
      window_stack_pop(window);
    }
    TripState = STATE_NONE;
    TripNum = 0;
    NumStations = 0;
    NumLegs = 0;
    
    if(Legs)
    {
      free(Legs);
    }
    
    if(Stations)
    {
      free(Stations);
    }
  }
}

void
trip_message_handler(DictionaryIterator *iter, void *context)
{
  Tuple *trips_key = dict_find(iter, KEY_TRIP);

  if (trips_key)
    {
      if (trips_key->value->int32 == 1)
        {
          Tuple *provider = dict_find(iter, KEY_PROVIDER);

          Tuple *start_id = dict_find(iter, KEY_START_ID);
          Tuple *stop_id = dict_find(iter, KEY_STOP_ID);

          if(provider->value->int32 != Provider)
            {
              TripState = STATE_ERROR;
            }
          else if(start_id->value->int32 != StartStation.id)
            {
              TripState = STATE_ERROR;
            }
          else if(stop_id->value->int32 != StopStation.id)
            {
              TripState = STATE_ERROR;
            }
          else
            {
              Tuple * lt = dict_find(iter, KEY_LOCALTIME);
              time_t offset = lt->value->int32 - time(NULL);
              APP_LOG(APP_LOG_LEVEL_DEBUG, "offset %d, servertime %d, time %d", (int ) offset, (int ) lt->value->int32, (int) time(NULL));
              APP_LOG(APP_LOG_LEVEL_DEBUG, "heap free %d, bytes used %d\n", heap_bytes_free(), heap_bytes_used());
              
              
              Tuple * numitems = dict_find(iter, KEY_NUMITEMS);
              int num = numitems->value->int32;
              NumStations = num;
              
              if(num > 0)
                {
                  if(Stations)
                    {
                      free(Stations);
                      Stations = 0;
                    }
                  Stations = malloc(sizeof(station)*num);

                  

                  uint8_t id_keys[num];
                  uint8_t name_keys[num];
                  uint8_t name2_keys[num];

                  Tuple* keys = dict_find(iter, KEY_STATION_NAME);
                  memcpy(name_keys, keys->value->data, num);

                  keys = dict_find(iter, KEY_STATION_ID);
                  memcpy(id_keys, keys->value->data, num);

                  keys = dict_find(iter, KEY_STATION_NAME2);
                  memcpy(name2_keys, keys->value->data, num);

                  int i;
                  for (i = 0; i < num; i++)
                    {
                      Tuple *stationID = dict_find(iter, id_keys[i]);
                      Tuple *stationName = dict_find(iter, name_keys[i]);
                      Tuple *stationName2 = dict_find(iter, name2_keys[i]);
                      strncpy(Stations[i].name, stationName->value->cstring, NAME_LENGTH);
                      strncpy(Stations[i].name2, stationName2->value->cstring, NAME_LENGTH);
                      Stations[i].id = stationID->value->int32;
                    }

                }
                
                numitems = dict_find(iter, KEY_NUMLEGS);
                num = numitems->value->int32;
                NumLegs = num;
              
                if(num > 0)
                  {
                    if(Legs)
                      {
                        free(Legs);
                        Legs = 0;
                      }
                    Legs = malloc(sizeof(leg)*num);
                    
                    uint8_t start_keys[num];
                    uint8_t stop_keys[num];
                    uint8_t startid_keys[num];
                    uint8_t stopid_keys[num];

                    uint8_t train_keys[num];
                    uint8_t destination_keys[num];
                    uint8_t platformStart_keys[num];
                    uint8_t platformStop_keys[num];
                    
                    Tuple* keys = dict_find(iter, KEY_LEG_START_TIME);
                    memcpy(start_keys, keys->value->data, num);
                    
                    keys = dict_find(iter, KEY_LEG_STOP_TIME);
                    memcpy(stop_keys, keys->value->data, num);

                    keys = dict_find(iter, KEY_LEG_START_ID);
                    memcpy(startid_keys, keys->value->data, num);
                    
                    keys = dict_find(iter, KEY_LEG_STOP_ID);
                    memcpy(stopid_keys, keys->value->data, num);

                    keys = dict_find(iter, KEY_LEG_START_PLATFORM);
                    memcpy(platformStart_keys, keys->value->data, num);
                    
                    keys = dict_find(iter, KEY_LEG_STOP_PLATFORM);
                    memcpy(platformStop_keys, keys->value->data, num);
                    
                    keys = dict_find(iter, KEY_LINE);
                    memcpy(train_keys, keys->value->data, num);
                    
                    keys = dict_find(iter, KEY_LINEDEST);
                    memcpy(destination_keys, keys->value->data, num);

                    int i;
                    for (i = 0; i < num; i++)
                      {
                        Tuple *startID = dict_find(iter, startid_keys[i]);
                        Tuple *stopID = dict_find(iter, stopid_keys[i]);
                        Tuple *start = dict_find(iter, start_keys[i]);
                        Tuple *stop = dict_find(iter, stop_keys[i]);
                        
                        Tuple *platformStart = dict_find(iter, platformStart_keys[i]);
                        Tuple *platformStop = dict_find(iter, platformStop_keys[i]);
                        Tuple *train = dict_find(iter, train_keys[i]);
                        Tuple *destination = dict_find(iter, destination_keys[i]);
                        
                        Legs[i].start = start->value->int32;
                        Legs[i].stop = stop->value->int32;
                        Legs[i].startID = startID->value->int32;
                        Legs[i].stopID = stopID->value->int32;

                        strncpy(Legs[i].train, train->value->cstring, LINE_LENGTH);
                        strncpy(Legs[i].destination, destination->value->cstring, NAME_LENGTH);
                        strncpy(Legs[i].startPlatform, platformStart->value->cstring, LINE_LENGTH);
                        strncpy(Legs[i].stopPlatform, platformStop->value->cstring, LINE_LENGTH);
                      }

                  }
              TripState = STATE_READY;
            }
        }
      else if (trips_key->value->int32 < 0)
        {
          TripState = trips_key->value->int32;
        }

      if(CurrentView == VIEW_TRIP)
        {
          //menu_layer_reload_data(menu_layer);
          //layer_mark_dirty(menu_layer_get_layer(menu_layer));
        }
    }
}

void
send_trip_request()
{
  if(JSReady == 0)
    return;

  APP_LOG(APP_LOG_LEVEL_DEBUG, "sending trip request");

  TripState = STATE_LOADING;

  Tuplet trips = TupletInteger(KEY_TRIP, 1);
  Tuplet tripnum = TupletInteger(KEY_TRIPNUM, TripNum);
  Tuplet start = TupletInteger(KEY_START_ID, StartStation.id);
  Tuplet stop = TupletInteger(KEY_STOP_ID, StopStation.id);
  Tuplet provider = TupletInteger(KEY_PROVIDER, Provider);

  DictionaryIterator *iter;
  app_message_outbox_begin(&iter);
  if (iter == NULL)
    {
      return;
    }

  dict_write_tuplet(iter, &trips);
  dict_write_tuplet(iter, &tripnum);
  dict_write_tuplet(iter, &start);
  dict_write_tuplet(iter, &stop);
  dict_write_tuplet(iter, &provider);
  dict_write_end(iter);

  app_message_outbox_send();
}

static void
window_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);


}


static void
window_disappear(Window *window)
{
  TripState = STATE_NONE;
}

static void
window_appear(Window *window)
{
  CurrentView = VIEW_TRIP;
  send_trip_request();

}

static void
window_unload(Window *window)
{

}


void
trip_show_window(int8_t tripnum)
{
  TripNum = tripnum;
  window_stack_push(window, true);
}

void
trip_init()
{
  TripState = STATE_LOADING;

  window = window_create();
  window_set_window_handlers(window, (WindowHandlers
        )
          { .load = window_load, .unload = window_unload, .appear = window_appear, .disappear = window_disappear, });
}

void
trip_deinit()
{
  window_destroy(window);
}
