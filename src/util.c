/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

int
digits(int num, char* buff, int fill)
{
  int i = 0, temp_num = num, length = 0;

  if (num >= 0)
    {
      // count how many characters in the number
      while (temp_num)
        {
          temp_num /= 10;
          length++;
        }

      if (length > 2)
        length = 2;

      if (fill)
        length = 2;

      for (; i < length; i++)
        {
          buff[(length - 1) - i] = '0' + (num % 10);
          num /= 10;
        }

      buff[i] = '\0'; // can't forget the null byte to properly end our string
    }

  return length;
}
