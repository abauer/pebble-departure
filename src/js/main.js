/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var XmlDocument = require('xmldoc').XmlDocument;

var maxTriesForSendingAppMessage = 3;
var timeoutForAppMessageRetry = 3000;

var MaxStations = 5;

function sendAppMessage(message, numTries) {
	if (numTries < maxTriesForSendingAppMessage) {
		numTries++;

		console.log('Sending AppMessage to Pebble: ' + JSON.stringify(message));
		Pebble.sendAppMessage(message, function() {
		}, function(e) {
			console.log('Failed sending AppMessage. Error: '
					+ e.data.error.message);
			setTimeout(function() {
				sendAppMessage(message, numTries);
			}, timeoutForAppMessageRetry);
		});
	} else {
		console.log('Failed sending AppMessage. Bailing. '
				+ JSON.stringify(message));
	}
}

function sendError(fun, error, e) {
	var message = {};
	message[fun] = -error;
	sendAppMessage(message, 0);
	console.log("Request returned error code " + error + "; function: " + fun
			+ "; " + e);
}

function getStationsGPSXML(position,url, provider) {
	var req = new XMLHttpRequest();
	var lat = position.coords.latitude;
	var lon = position.coords.longitude;

	var url = url + 'XML_COORD_REQUEST?coordOutputFormat=WGS84&max='
			+ MaxStations + '&inclFilter=1&radius_1=1320&type_1=STOP&coord='
			+ lon + ':' + lat + ':WGS84'
	req.open('GET', url, true);
	console.log(url);

	req.onload = function(e) {
		if (req.status == 200) {
			response = new XmlDocument(req.responseText);
			try {
				var num = 0;
				var results = response.childNamed('itdCoordInfoRequest')
						.childNamed('itdCoordInfo').childNamed(
								'coordInfoItemList')
				if (results == undefined) {
					num = 0;
				} else {
					results = results.childrenNamed('coordInfoItem');
					num = results.length;
					if (num > MaxStations) {
						num = MaxStations;
					}
				}

				var message = {};
				message.stations = 1;
				message.numitems = num;
				message.station_name = new Array(num);
				message.station_name2 = new Array(num);
				message.station_id = new Array(num);
				message.provider = provider;

				for ( var i = 0; i < num; i++) {
					message.station_name[i] = 100 + i;
					message.station_name2[i] = 150 + i;
					message.station_id[i] = 200 + i;
					message[100 + i] = results[i].attr.name.substr(0, 25);
					message[150 + i] = results[i].attr.locality.substr(0, 25);
					message[200 + i] = parseInt(results[i].attr.id);

				}
				sendAppMessage(message, 0);
			} catch (e) {
				sendError(1, 3, e);
			}
		} else {
			sendError(1, 2, 0);
		}
	};

	req.onerror = function(e) {
		sendError(1, 1, 0);
	};

	req.send(null);

}

function getStationLocateXML(url, provider) {
    function tmp(position) {
        getStationsGPSXML(position, url, provider);
    };
	navigator.geolocation.getCurrentPosition(tmp);
}


function getStationsGPSJSON(position, url, provider) {
    var req = new XMLHttpRequest();
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;

    var url = url + 'XML_COORD_REQUEST?outputFormat=JSON&coordOutputFormat=WGS84&max=' + MaxStations + '&inclFilter=1&radius_1=1320&type_1=STOP&coord=' + lon + ':' + lat + ':WGS84'
    req.open('GET', url, true);
    console.log(url);

    req.onload = function(e) {
        if (req.status == 200) {
            try {
                response = JSON.parse(req.responseText);

                var num = 0;
                num = response.pins.length;

                var message = {};
                message.stations = 1;
                message.numitems = num;
                message.station_name = new Array(num);
                message.station_name2 = new Array(num);
                message.station_id = new Array(num);
                message.provider = provider;

                for (var i = 0; i < num; i++) {
                    message.station_name[i] = 100 + i;
                    message.station_name2[i] = 150 + i;
                    message.station_id[i] = 200 + i;
                    message[100 + i] = response.pins[i].desc.substr(0, 25);
                    message[150 + i] = response.pins[i].locality.substr(0, 25);
                    message[200 + i] = parseInt(response.pins[i].id);

                }
                sendAppMessage(message, 0);
            } catch (e) {
                sendError(1, 3, e);
            }
        } else {
            sendError(1, 2, 0);
        }
    };

    req.onerror = function(e) {
        sendError(1, 1, 0);
    };

    req.send(null);
}


function getStationLocateJSON(url, provider) {
    function tmp(position) {
        getStationsGPSJSON(position, url, provider);
    };
	navigator.geolocation.getCurrentPosition(tmp);
}

function getStations(url, provider, stations_method) {
    if(stations_method == 0) {
        getStationLocateJSON(url, provider);   
    }
    else if(stations_method == 1)
    {
        getStationLocateXML(url, provider);
    }
    else
    {
        sendError(1,5);
    }
}


function getStationsNameJSON(url, provider, val, num) {
    var req = new XMLHttpRequest();
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;

    var url = url + 'XML_STOPFINDER_REQUEST?outputFormat=JSON&locationServerActive=1&type_sf=any&name_sf='+val;
    req.open('GET', url, true);
    console.log(url);

    req.onload = function(e) {
        if (req.status == 200) {
            try {
                response = JSON.parse(req.responseText);

                try{
                var message = {};
                message.settings = 1;
                message.station_name = response.stopFinder.points.object;
                message.station_name2 = response.stopFinder.points.posttown;
                message.station_id = response.stopFinder.points.stateless;
                message.station_num = num;
                message.provider = provider;
            }
            catch(e)
            {console.log(response);}

                sendAppMessage(message, 0);
            } catch (e) {
                sendError(31, 3, e);
            }
        } else {
            sendError(31, 2, 0);
        }
    };

    req.onerror = function(e) {
        sendError(31, 1, 0);
    };

    req.send(null);
}

function getStationsName(url, provider, stations2_method, val, num) {
    console.log("Url "+url+"provider" +provider+" stantions method "+stations2_method+" val "+val);
    if(stations2_method == 0) {
        console.log("blubb")
        getStationsNameJSON(url, provider, val, num);   
    }
    else if(stations2_method == 1)
    {
        sendError(31,6);
        //getStationsNameXML(url, provider, val, nu);
    }
    else
    {
        sendError(31,5);
    }
}


function getDeparturesXML(url, provider, stopID) {
    var req = new XMLHttpRequest();
    var url = url +'XML_DM_REQUEST?&sessionID=0&depType=stopEvents&mode=direct&limit=10&mergeDep=1&useRealtime=1&line=any&type_dm=stationID&name_dm=' + stopID;
    req.open('GET', url, true);
    console.log(url);

    req.onload = function(e) {
        if (req.status == 200) {
            response = new XmlDocument(req.responseText);
            var num = 0;
            try {

                var results = response.childNamed('itdDepartureMonitorRequest')
                    .childNamed('itdDepartureList');
                if (results == undefined) {
                    num = 0;
                } else {
                    results = results.childrenNamed('itdDeparture');
                    num = results.length;
                    if (num > 10) {
                        num = 10;
                    }
                }

                var message = {};
                message.departures = 1;
                message.numitems = num;
                message.departure_line = new Array(num);
                message.departure_time = new Array(num);
                message.departure_dest = new Array(num);
                message.departure_rt = new Array(num);
                message.provider = provider;
                message.station_id = stopID;

                var a = response.attr.now.split(/[^0-9]/);
                var lt = new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);

                message.localtime = Math.round(lt.getTime() / 1000);

                for (var i = 0; i < num; i++) {

                    var date;
                    var time;
                    try {
                        tm = parseDateTime(results[i].childNamed('itdRTDateTime'));
                        message.departure_rt[i] = 1;
                    } catch (e) {
                        tm = parseDateTime(results[i].childNamed('itdDateTime'));
                        message.departure_rt[i] = 0;
                    }

                    var line = results[i].childNamed('itdServingLine').attr.number;
                    var dest = results[i].childNamed('itdServingLine').attr.direction
                        .substr(0, 25);


                    message.departure_line[i] = 100 + i;
                    message.departure_time[i] = 150 + i;
                    message.departure_dest[i] = 200 + i;

                    message[100 + i] = line;
                    message[150 + i] = Math.round(tm.getTime() / 1000);
                    message[200 + i] = dest;

                }
                sendAppMessage(message, 0);
            } catch (e) {
                sendError(4, 3, e);
            }

        } else {
            sendError(4, 2, 0);
        }

    };

    req.onerror = function(e) {
        sendError(4, 1, 0);
    };

    req.send(null);

}


function getDepartures(url, provider, departure_method, station_id) {
    console.log("in getDepartures() url " + url+" provider "+provider+" method "+departure_method);
    if(departure_method == 0) {
        getDeparturesJSON(url, provider);   
    }
    else if(departure_method == 1)
    {
        getDeparturesXML(url, provider, station_id);
    }
    else
    {
        sendError(4,5);
    }
}


function processPoint(context, point) {
    stopID = point.attr.stopID;
    if (!context.stops[stopID]) {
        st = {};
        st.locality = point.attr.locality;
        st.name = point.attr.nameWO;
        context.stops[stopID] = st;
    }
}

function parseDateTime(node) {
    date = node.childNamed('itdDate');
    time = node.childNamed('itdTime');

    var tm = new Date();

    tm.setFullYear(parseInt(date.attr.year));
    tm.setMonth(parseInt(date.attr.month) - 1);
    tm.setDate(parseInt(date.attr.day));

    tm.setHours(parseInt(time.attr.hour));
    tm.setMinutes(parseInt(time.attr.minute));
    tm.setSeconds(0);

    return tm;
}



var context_global;

/*
function getTrips(startID, stopID) {
    var t1 = new Date();
	var req = new XMLHttpRequest();
	var date = new Date();
	var hr = date.getHours();
	if (hr < 10)
		hr = "0"+hr;
	var min = date.getMinutes();
	if (min < 10)
		min = "0" + min;
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	if (month < 10)
		month = "0" + month;
	var day = date.getDate();
	if (day < 10)
		day = "0" + day;
	
	var date_str = year.toString() + month.toString() + day.toString();
	var time_str = hr.toString() + min.toString();
	console.log("time "+time_str);
	console.log(date.toString());

	var url = Urls[provider]
			+ 'XSLT_TRIP_REQUEST2?outputFormat=XML&&coordOutputFormat=WGS84&sessionID=0&requestID=0&language=de&itdDate='
			+ date_str
			+ '&itdTime='
			+ time_str
			+ '&itdTripDateTimeDepArr=dep&useRealtime=1&nextDepsPerLeg=0&type_origin=stop&name_origin='
			+ startID + '&type_destination=stop&name_destination=' + stopID;
	req.open('GET', url, true);
	console.log(url);

	req.onload = function(e) {
		if (req.status == 200) {
		    var t2 = new Date();
			response = new XmlDocument(req.responseText);
			var num = 0;
		    var t3 = new Date();
			try {

				var sessionID = response.attr.sessionID;
				console.log('sessionID '+sessionID);
				var results = response.childNamed('itdTripRequest')
				var requestID = results.attr.requestID;
				console.log('requestID ' +requestID);
				
				var context = {};
				context.sessionID = sessionID;
				context.requestID = requestID;
				
				context.stops = {};
				

				results = results.childNamed('itdItinerary');

				if (results == undefined) {
					num = 0;
				} else {
					results = results.childNamed('itdRouteList').childrenNamed(
							'itdRoute');
					num = results.length;
				}
				
				context.trips = new Array(num)
				
				for(var i = 0; i < num; i++)
					{
					
					rt = results[i];
					var trip = {};
					
					trip.changes = rt.attr.changes
					
					var rss = rt.childNamed('itdPartialRouteList').childrenNamed('itdPartialRoute');
					var nlegs = rss.length;
					trip.legs = new Array(nlegs);
					
					for(var j = 0; j < nlegs; j++)
					{
						var rs = rss[j];
						leg = {};
						
						leg.type = rs.attr.type
						
						var start = rs.childrenNamed('itdPoint')[0];
						if(start.attr.usage != "departure")
							{
							throw {};
							}
						leg.startTime = parseDateTime(start.childNamed('itdDateTime'));
						try
						{
							leg.startTimeTarget = parseDateTime(start.childNamed('itdDateTimeTarget'));
						}
						catch(e)
						{
							leg.startTimeTarget = leg.startTime;
						}
						leg.startID = start.attr.stopID;
						leg.startPlatform = start.attr.platformName;
						processPoint(context, start);

						var stop = rs.childrenNamed('itdPoint')[1];
						if(stop.attr.usage != "arrival")
							{
							throw {};
							}
						leg.stopTime = parseDateTime(stop.childNamed('itdDateTime'));
						leg.stopTimeTarget = parseDateTime(stop.childNamed('itdDateTimeTarget'));
						leg.stopID = stop.attr.stopID;
						leg.stopPlatform = stop.attr.platformName;
						processPoint(context, stop);
						
						leg.train = rs.childNamed('itdMeansOfTransport').attr.shortname;
						leg.destination = rs.childNamed('itdMeansOfTransport').attr.destination;
						
						trip.legs[j] = leg;
						
					}
					
					trip.start = trip.legs[0].startTime
					trip.end = trip.legs[nlegs-1].stopTime;
					
					context.trips[i] = trip;
					}
				
				//console.log(JSON.stringify(context));
				
				context_global = context;
				var t4 = new Date();

				var message = {};
				message.trips = 1;
				message.numitems = num;
				message.trips_time = new Array(num);
				message.trips_duration = new Array(num);
				message.trips_changes = new Array(num);

				var a = response.attr.now.split(/[^0-9]/);
				var lt = new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);

				message.localtime = Math.round(lt.getTime() / 1000);
				message.provider = provider;
				message.start_id = startID;
				message.stop_id = stopID;

				for ( var i = 0; i < num; i++) {

					message.trips_time[i] = 100 + i;
					message.trips_duration[i] = 150 + i;
					message.trips_changes[i] = 200 + i;

					message[100 + i] = Math.round(context.trips[i].start.getTime() / 1000);
					message[150 + i] = Math.round(context.trips[i].end.getTime() / 1000)-Math.round(context.trips[i].start.getTime() / 1000);
					message[200 + i] = context.trips[i].changes;

				}

				sendAppMessage(message, 0);
		    var t5 = new Date();

		    console.log("dt1 " + (t2.valueOf()-t1.valueOf()));
		    console.log(" dt2 " + (t3.valueOf()-t2.valueOf()));
		    console.log(" dt3 " + (t4.valueOf()-t3.valueOf()));
		    console.log(" dt4 " + (t5.valueOf()-t4.valueOf()));
			} catch (e) {
				sendError(14, 3, e);
			}

		} else {
			sendError(14, 2, 0);
		}

	};

	req.onerror = function(e) {
		sendError(14, 1, 0);
	};

	req.send(null);

}
*/
function processPointJSON(context, point)
{
	var stopID = parseInt(point.ref.id);
	if(!context.stops[stopID])
	{
		var st = {};

		if(point.locality) {
			st.locality = point.locality;
		} else {
			st.locality = point.place;
		}
			
		if(point.nameWO) {
			st.name = point.nameWO;
		} else {
			name = point.name.replace(st.locality,"").replace(", " , "");
			st.name = name;
		}

		context.stops[stopID] = st;
	}
	}
function parseDateTimeJSON(node)
{
	date = node.date;
	time = node.time;

	var a = date.split(/[^0-9]/);
	var b = time.split(/[^0-9]/);

	var tm = new Date();
	tm.setFullYear(parseInt(a[2]));
	tm.setMonth(parseInt(a[1]));
	tm.setDate(parseInt(b[0]));

	tm.setHours(parseInt(b[0]));
	tm.setMinutes(parseInt(b[1]));
	tm.setSeconds(0);
	
	return Math.round(tm.getTime() / 1000);
	}

function getTripsJSON(startID, stopID) {
	var req = new XMLHttpRequest();
	var date = new Date();
	var hr = date.getHours();
	if (hr < 10)
		hr = "0"+hr;
	var min = date.getMinutes();
	if (min < 10)
		min = "0" + min;
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	if (month < 10)
		month = "0" + month;
	var day = date.getDate();
	if (day < 10)
		day = "0" + day;
	
	var date_str = year.toString() + month.toString() + day.toString();
	var time_str = hr.toString() + min.toString();

	var url = Urls[provider]
			+ 'XSLT_TRIP_REQUEST2?outputFormat=JSON&&coordOutputFormat=WGS84&sessionID=0&requestID=0&language=de&itdDate='
			+ date_str
			+ '&itdTime='
			+ time_str
			+ '&itdTripDateTimeDepArr=dep&useRealtime=1&nextDepsPerLeg=0&type_origin=stop&name_origin='
			+ startID + '&type_destination=stop&name_destination=' + stopID;
	req.open('GET', url, true);
	console.log(url);

	req.onload = function(e) {
		if (req.status == 200) {
		    var response = JSON.parse(req.responseText);
			var num = 0;
			try {

				var sessionID = response.parameters[1].value;
				console.log('sessionID '+sessionID);

				var requestID = response.parameters[0].value;
				console.log('requestID ' +requestID);
				
				var context = {};
				context.sessionID = sessionID;
				context.requestID = requestID;
				
				context.stops = {};
				
				

				//if (results == undefined) {
				//	num = 0;
				//} else {
					num = response.trips.length;
				//}
				
				context.trips = new Array(num)
				
				for(var i = 0; i < num; i++)
					{
					
					rt = response.trips[i].trip;
					var trip = {};
					
					trip.changes = parseInt(rt.interchange);
					

					var nlegs = rt.legs.length;
					trip.legs = new Array(nlegs);
					
					for(var j = 0; j < nlegs; j++)
					{
						var rs = rt.legs[j];
						
						leg = {};
						
						//leg.type = rs.attr.type
						
						var start = rs.points[0];
						if(start.usage != "departure")
							{
							throw {};
							}
						leg.startTime = parseDateTimeJSON(start.dateTime);
						//try
						//{
						//	leg.startTimeTarget = parseDateTime(start.childNamed('itdDateTimeTarget'));
						//}
						///catch(e)
						//{
							leg.startTimeTarget = leg.startTime;
						//}
						leg.startID = parseInt(start.ref.id);
						leg.startPlatform = start.ref.platform;
						processPointJSON(context, start);

						var stop = rs.points[1];
						if(stop.usage != "arrival")
							{
							throw {};
							}
						leg.stopTime = parseDateTimeJSON(stop.dateTime);
						//leg.stopTimeTarget = parseDateTime(stop.childNamed('itdDateTimeTarget'));
						leg.stopTimeTarget = leg.stopTime;
						leg.stopID = parseInt(stop.ref.id);
						leg.stopPlatform = stop.ref.platform;
						processPointJSON(context, stop);
						
						leg.train = rs.mode.number;
						leg.destination = rs.mode.destination;
						
						trip.legs[j] = leg;
						
					}
					
					trip.start = trip.legs[0].startTime
					trip.end = trip.legs[nlegs-1].stopTime;
					
					context.trips[i] = trip;
					}
				
				console.log(JSON.stringify(context));
				
				var lt = new Date();
				context.localtime = Math.round(lt.getTime() / 1000);
				context.provider = provider;
				context.startID = startID;
				context.stopID = stopID;
				
				context_global = context;

				var message = {};
				message.trips = 1;
				message.numitems = num;
				message.trips_time = new Array(num);
				message.trips_duration = new Array(num);
				message.trips_changes = new Array(num);
				
				message.localtime = context.localtime;
				message.provider = provider;
				message.start_id = startID;
				message.stop_id = stopID;

				for ( var i = 0; i < num; i++) {

					message.trips_time[i] = 100 + i;
					message.trips_duration[i] = 150 + i;
					message.trips_changes[i] = 200 + i;

					message[100 + i] = context.trips[i].start;
					message[150 + i] = context.trips[i].end;
					message[200 + i] = context.trips[i].changes;

				}

				sendAppMessage(message, 0);
			} catch (e) {
				sendError(14, 3, e);
			}

		} else {
			sendError(14, 2, 0);
		}

	};

	req.onerror = function(e) {
		sendError(14, 1, 0);
	};

	req.send(null);

}

function getTrip(tripnum)
{
	try
	{
		var message = {};

		message.trip = 1;


		trip = context_global.trips[tripnum];

		stations = {};

		for( var i = 0; i < trip.legs.length; i++)
		{
			st = trip.legs[i].startID;
			//if(!stations[st])
			stations[st] = context_global.stops[st];

			st = trip.legs[i].stopID;
			//if(!stations[st])
			stations[st] = context_global.stops[st];
		}

		num = Object.keys(stations).length;
		message.numitems = num;
		message.station_id = new Array(num);
		message.station_name =  new Array(num);
		message.station_name2 = new Array(num);

		var i = 0;
		for(var key in stations) {
			if (stations.hasOwnProperty(key)) {
				message.station_name[i] = 100 + i;
				message.station_name2[i] = 150 + i;
				message.station_id[i] = 200 + i;
				message[200 + i] = parseInt(key);
				message[100 + i] = stations[key].name;
				message[150 + i] = stations[key].locality;
				i = i+1;
			}
		}
	  	
	  	num = trip.legs.length;
	  	message.numlegs = num
	    message.leg_line = new Array(num);
	  	message.leg_linedest = new Array(num);
	  	message.leg_start_platform = new Array(num);
	  	message.leg_stop_platform = new Array(num);
	  	message.leg_startID = new Array(num);
	  	message.leg_stopID = new Array(num);
	  	message.leg_start_time = new Array(num);
	  	message.leg_stop_time = new Array(num);
		for(var i = 0; i < trip.legs.length; i++)
			{
			  message.leg_line[i] = 300 + i;
		  	message.leg_linedest[i] = 400 + i;
		  	message.leg_start_platform[i] = 500 + i;
		  	message.leg_stop_platform[i] = 600 + i;
		  	message.leg_startID[i] = 700 + i;
		  	message.leg_stopID[i] = 800 + i;
		  	message.leg_start_time[i] = 900 + i;
		  	message.leg_stop_time[i] = 1000 + i;
		  	
		  	message[300 + i] = trip.legs[i].train;
		  	message[400 + i] = trip.legs[i].destination;
		  	message[500 + i] = trip.legs[i].platformStart;
		  	message[600 + i] = trip.legs[i].platformStop;
		  	message[700 + i] = trip.legs[i].startID;
		  	message[800 + i] = trip.legs[i].stopID;
		  	message[900 + i] = trip.legs[i].startTime;
		  	message[1000 + i] = trip.legs[i].stopTime;
		  	
			}

		message.localtime = context_global.localtime;
		message.provider = context_global.provider;
		message.start_id = context_global.startID;
		message.stop_id = context_global.stopID;
		console.log(JSON.stringify(message));
		sendAppMessage(message, 0);

	} catch (e) {
		sendError(20, 3, e);
	}
}

Pebble.addEventListener('appmessage', function(e) {
    console.log('AppMessage received from Pebble: ' + JSON.stringify(e.payload));
    
    provider = parseInt(window.localStorage.getItem("provider"));
    url = window.localStorage.getItem("url");
    
    stations_method = parseInt(window.localStorage.getItem("stations_method"));
    departure_method = parseInt(window.localStorage.getItem("departure_method"));

    provider_msg = e.payload.provider;
    if(provider_msg != provider) {
        sendError(10,1);
    }
    
    var request = e.payload.stations;
    if (request) {
        getStations(url, provider, stations_method);
        return;
    }
    var dm = e.payload.departures;
    if (dm) {
        getDepartures(url, provider, departure_method, e.payload.station_id);
        return;
    }

    var trips = e.payload.trips;
    if (trips) {
        provider = e.payload.provider;
        getTripsJSON(e.payload.start_id, e.payload.stop_id);
        return;
    }

    var trip = e.payload.trip;
    if (trip) {
        getTrip(e.payload.tripnum);
        return;
    }

});


Pebble.addEventListener("ready", function(e) {
  console.log("at ready");
	var message = {};
	message.jsready = 1;
	sendAppMessage(message, 0);
});


Pebble.addEventListener("showConfiguration", function() {
  console.log("showing configuration");
  var options = {};
  
  var options = window.localStorage.getItem("options");
  console.log("Options = " + options);
  console.log('http://192.168.2.104:8000/settings.html?options='+encodeURIComponent(options));
  Pebble.openURL('http://192.168.2.104:8000/settings.html?options='+encodeURIComponent(JSON.stringify(options)));
});

Pebble.addEventListener("webviewclosed", function(e) {
    console.log("configuration closed");

    if (e.response.charAt(0) == "{" && e.response.slice(-1) == "}" && e.response.length > 5) {
        options = JSON.parse(decodeURIComponent(e.response));
        console.log("Options = " + JSON.stringify(options));

        var url = options['url'];
        var provider = parseInt(options['provider']);
        var stations_method = parseInt(options['stations_method']);
        var stations2_method = parseInt(options['stations2_method']);
        var departure_method = parseInt(options['departure_method']);

        window.localStorage.setItem("options", JSON.stringify(options));
        window.localStorage.setItem("provider", provider);
        window.localStorage.setItem("url", url);

        window.localStorage.setItem("stations_method", stations_method);
        window.localStorage.setItem("stations_method2", stations2_method);
        window.localStorage.setItem("departure_method", departure_method);

        var message = {};
        message.settings = 1;
        message.provider = parseInt(options['provider']);
        message.view = parseInt(options['view']);


        sendAppMessage(message, 0);

        for (var i = 0; i < options['stops'].length; i++) {
            if (i >= MaxStations) {
                return;
            }

            var val = options['stops'][i];
            if (val != "") {
                getStationsName(url, provider, stations2_method, val, i);
            }
        }

    } else {
        console.log("Cancelled");
    }

});
