/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define NAME_LENGTH 26
#define LINE_LENGTH 20
#define TIME_LENGTH 10

#define MAX_STATIONS 10
#define MAX_DEPARTURES 10

#define MAX_TRIPS 5

#define REFRESH_INTERVAL 30

#define STATE_NONE 0
#define STATE_READY 1
#define STATE_LOADING 2
#define STATE_ERROR -1

typedef struct
{
  int32_t id;
  char name[NAME_LENGTH];
  char name2[NAME_LENGTH];
} station;

typedef struct
{
  char line[LINE_LENGTH];
  char dest[NAME_LENGTH];
  char time_str[TIME_LENGTH];
  int8_t rt;
  time_t time;
} departure;

extern station CurrentStation;
extern station SavedStations[MAX_STATIONS];
extern station NearbyStations[MAX_STATIONS];

extern int8_t DepartureState;
extern int Provider;

extern int8_t NumSavedStations;
extern int8_t NumNearbyStations;

extern departure Departures[MAX_DEPARTURES];
extern int8_t NumDepartures;

extern time_t LastUpdateDepartures;
extern int8_t ViewMode;

extern int8_t StationsState;

extern int8_t JSReady;
extern int8_t CurrentView;

extern station StartStation;
extern station StopStation;

void
departure_reset(int8_t mode);
void
departure_update_window();
void
departure_list_mode();
void
departure_set_station(station* new_station);
void
save_current_station();
void
send_departure_request();
void
departure_show_window();
void
departure_init();
void
departure_deinit();
void
departure_message_handler(DictionaryIterator *iter, void *context);

void
stations_reset(int8_t mode);
void
stations_show_window(int8_t view);
void
stations_init();
void
stations_deinit();
void
stations_message_handler(DictionaryIterator *iter, void *context);
void send_stations_request();

void
about_show_window();
void
about_init();
void
about_deinit();

void
main_view_show_window();
void
main_view_init();
void
main_view_deinit();

void
trips_reset(int8_t mode);
void
trip_set_station_start(station* new_station);
void
trip_set_station_stop(station* new_station);
void
send_trips_request();
void
trips_message_handler(DictionaryIterator *iter, void *context);
void
trips_init();
void
trips_deinit();
void
trips_show_window();

void
trip_reset(int8_t mode);
void
trip_message_handler(DictionaryIterator *iter, void *context);
void
trip_deinit();
void
trip_init();
void
trip_show_window(int8_t tripnum);

char*
itoa(int num, char* buff);

int
digits(int num, char* buff, int fill);

enum
{
  KEY_DUMMY = 0x0,
  KEY_STATIONS = 0x1,
  KEY_STATION_ID = 0x2,
  KEY_STATION_NAME = 0x3,
  KEY_STATION_NAME2 = 0x9,
  KEY_DEPARTURES = 0x4,
  KEY_DEPARTURE_LINE = 0x5,
  KEY_DEPARTURE_TIME = 0x6,
  KEY_DEPARTURE_DEST = 0x7,
  KEY_NUMITEMS = 0x8,
  KEY_LOCALTIME = 0xa,
  KEY_PROVIDER = 0xb,
  KEY_JSREADY = 0xc,
  KEY_DEPARTURE_RT = 0xd,

  KEY_TRIPS = 0xe,
  KEY_START_ID = 0xf,
  KEY_STOP_ID = 0x10,
  KEY_TRIPS_START = 0x11,
  KEY_TRIPS_DURATION = 0x12,
  KEY_TRIPS_CHANGES = 0x13,

  KEY_TRIP = 0x14,
  KEY_TRIPNUM = 0x15,
  KEY_NUMLEGS = 0x16,
  KEY_LINE = 0x17,
  KEY_LINEDEST = 0x18,
  KEY_LEG_START_PLATFORM = 0x19,
  KEY_LEG_STOP_PLATFORM = 0x1a,
  KEY_LEG_START_ID = 0x1b,
  KEY_LEG_STOP_ID = 0x1c,
  KEY_LEG_START_TIME = 0x1d,
  KEY_LEG_STOP_TIME = 0x1e,
  
  KEY_SETTINGS = 0x1f,
  KEY_VIEW = 0x20,
};

enum
{
  VIEW_NONE = 0x0,
  VIEW_DEPARTURE = 0x1,
  VIEW_STATIONS1 = 0x2,
  VIEW_STATIONS2 = 0x3,
  VIEW_STATIONS3 = 0x4,
  VIEW_STATIONS4 = 0x9,
  VIEW_STATIONS5 = 0xa,
  VIEW_NETWORK1 = 0x5,
  VIEW_NETWORK2 = 0x6,
  VIEW_MAIN = 0x7,
  VIEW_TRIPS = 0x8,
  VIEW_TRIP = 0x9,
};

enum
{
  VIEW_SHORT = 0x0,
  VIEW_LONG = 0x1,
};
