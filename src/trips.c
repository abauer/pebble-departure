/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>

#include <departure.h>


station StartStation;
station StopStation;


int NumTrips = 0;

int8_t TripsState = STATE_NONE;

typedef struct
{
  time_t start;
  time_t duration;
  int8_t changes;
} trip;

trip *Trips = 0;

void
trip_set_station_start(station* new_station)
{
  memcpy(&StartStation, new_station, sizeof(station));
  TripsState = STATE_LOADING;
  NumTrips = 0;
}

void
trip_set_station_stop(station* new_station)
{
  memcpy(&StopStation, new_station, sizeof(station));
  TripsState = STATE_LOADING;
  NumTrips = 0;
}



static Window *window;
static MenuLayer *menu_layer;

void
trips_reset(int8_t mode)
{
  if(mode == 1)
  {
    if(CurrentView == VIEW_TRIPS)
    {
      window_stack_pop(window);
    }
    TripsState = STATE_NONE;
    NumTrips = 0;
    
    if(Trips)
    {
      free(Trips);
    }
  }
}

void
send_trips_request()
{
  if(JSReady == 0)
    return;

  APP_LOG(APP_LOG_LEVEL_DEBUG, "sending trip request");

  TripsState = STATE_LOADING;

  Tuplet trips = TupletInteger(KEY_TRIPS, 1);
  Tuplet start = TupletInteger(KEY_START_ID, StartStation.id);
  Tuplet stop = TupletInteger(KEY_STOP_ID, StopStation.id);
  Tuplet provider = TupletInteger(KEY_PROVIDER, Provider);

  DictionaryIterator *iter;
  app_message_outbox_begin(&iter);
  if (iter == NULL)
    {
      return;
    }

  dict_write_tuplet(iter, &trips);
  dict_write_tuplet(iter, &start);
  dict_write_tuplet(iter, &stop);
  dict_write_tuplet(iter, &provider);
  dict_write_end(iter);

  app_message_outbox_send();
}

void
trips_message_handler(DictionaryIterator *iter, void *context)
{
  Tuple *trips_key = dict_find(iter, KEY_TRIPS);

  if (trips_key)
    {
      if (trips_key->value->int32 == 1)
        {
          Tuple *provider = dict_find(iter, KEY_PROVIDER);

          Tuple *start_id = dict_find(iter, KEY_START_ID);
          Tuple *stop_id = dict_find(iter, KEY_STOP_ID);

          if(provider->value->int32 != Provider)
            {
              TripsState = STATE_ERROR;
            }
          else if(start_id->value->int32 != StartStation.id)
            {
              TripsState = STATE_ERROR;
            }
          else if(stop_id->value->int32 != StopStation.id)
            {
              TripsState = STATE_ERROR;
            }
          else
            {
              Tuple * numitems = dict_find(iter, KEY_NUMITEMS);
              NumTrips = numitems->value->int32;
              if (NumTrips > MAX_TRIPS)
                NumTrips = 5;


              if(NumTrips > 0)
                {
                  if(Trips)
                    {
                      free(Trips);
                      Trips = 0;
                    }
                  Trips = malloc(sizeof(trip)*NumTrips);

                  Tuple * lt = dict_find(iter, KEY_LOCALTIME);
                  time_t offset = lt->value->int32 - time(NULL);
                  //APP_LOG(APP_LOG_LEVEL_DEBUG, "offset %d, servertime %d, time %d", (int ) offset, (int ) lt->value->int32, (int) time(NULL));
                  APP_LOG(APP_LOG_LEVEL_DEBUG, "heap free %d, bytes used %d\n", heap_bytes_free(), heap_bytes_used());

                  uint8_t start_keys[NumTrips];
                  uint8_t duration_keys[NumTrips];
                  uint8_t changes_keys[NumTrips];

                  Tuple* keys = dict_find(iter, KEY_TRIPS_START);
                  memcpy(start_keys, keys->value->data, NumTrips);

                  keys = dict_find(iter, KEY_TRIPS_DURATION);
                  memcpy(duration_keys, keys->value->data, NumTrips);

                  keys = dict_find(iter, KEY_TRIPS_CHANGES);
                  memcpy(changes_keys, keys->value->data, NumTrips);

                  int i;
                  for (i = 0; i < NumTrips; i++)
                    {
                      Tuple *start = dict_find(iter, start_keys[i]);
                      Tuple *duration = dict_find(iter, duration_keys[i]);
                      Tuple *changes = dict_find(iter, changes_keys[i]);


                      Trips[i].start = start->value->int32-offset;
                      Trips[i].duration = duration->value->int32;
                      Trips[i].changes = changes->value->int32;
                    }

                }
              TripsState = STATE_READY;
            }
        }
      else if (trips_key->value->int32 < 0)
        {
          TripsState = trips_key->value->int32;
        }

      if(CurrentView == VIEW_TRIPS)
        {
          menu_layer_reload_data(menu_layer);
          layer_mark_dirty(menu_layer_get_layer(menu_layer));
        }
    }
}



static void
menu_select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data)
{

  if(cell_index->section == 0 && TripsState == STATE_READY && NumTrips > 0)
    {
      trip_show_window(cell_index->row);
    }
  else if(cell_index->section == 1 && cell_index->row == 0)
    {
      stations_show_window(VIEW_STATIONS4);
    }
  else if(cell_index->section == 2 && cell_index->row == 0)
    {
      stations_show_window(VIEW_STATIONS5);
    }

}

// A callback is used to specify the amount of sections of menu items
// With this, you can dynamically add and remove sections
static uint16_t
menu_get_num_sections_callback(MenuLayer *menu_layer, void *data)
{
  return 3;
}

// Each section has a number of items;  we use a callback to specify this
// You can also dynamically add and remove items using this
static uint16_t
menu_get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data)
{
  switch (section_index)
    {
  case 2:
  case 1:
    return 1;
    break;
  case 0:
    if ((TripsState == STATE_LOADING) || TripsState < 0 || (TripsState == STATE_READY && NumTrips == 0) || (JSReady==0) )
      {
        return 1;
      }
    else
      {
        return NumTrips;
      }
  default:
    return 0;
    }
}

// A callback is used to specify the height of the section header
static int16_t
menu_get_header_height_callback(MenuLayer *menu_layer, uint16_t section_index, void *data)
{
  // This is a define provided in pebble.h that you may use for the default height
  return MENU_CELL_BASIC_HEADER_HEIGHT;
}

// Here we draw what each header is
static void
menu_draw_header_callback(GContext* ctx, const Layer *cell_layer, uint16_t section_index, void *data)
{
  // Determine which section we're working with
  switch (section_index)
    {
  case 1:
    menu_cell_basic_header_draw(ctx, cell_layer, "From");
    break;
  case 2:
    menu_cell_basic_header_draw(ctx, cell_layer, "To");
    break;
  case 0:
    menu_cell_basic_header_draw(ctx, cell_layer, "Connections");
    break;
    }
}

void
gen_string1(time_t start, time_t duration, char* buffer, size_t size)
{
  time_t end = start + duration;

  int pos = 0;
  struct tm *ts = localtime(&start);
  pos += strftime(buffer,size, "%H:%M", ts);

  buffer[pos++] = ' ';
  buffer[pos++] = '-';
  buffer[pos++] = ' ';

  struct tm *te = localtime(&end);
  pos += strftime(buffer+pos,size-pos, "%H:%M", te);

  buffer[pos++] = '\0';
}

void
gen_string2(trip* tr, char* buffer, size_t size)
{
  //APP_LOG(APP_LOG_LEVEL_DEBUG, "changes %d",tr->changes);
  snprintf(buffer,size, "changes: %d",tr->changes);
}

// This is the menu item draw callback where you specify what each item should look like
static void
menu_draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data)
{
  // Determine which section we're going to draw in
  switch (cell_index->section)
    {
  case 1:
    menu_cell_basic_draw(ctx, cell_layer, StartStation.name, StartStation.name2, NULL);
    break;
  case 2:
    menu_cell_basic_draw(ctx, cell_layer, StopStation.name, StopStation.name2, NULL);
    break;

  case 0:
    if(JSReady == 0)
      {
        menu_cell_basic_draw(ctx, cell_layer, "connecting...", "", NULL);
        return;
      }
    if(TripsState == STATE_LOADING)
      menu_cell_basic_draw(ctx, cell_layer, "loading...", "", NULL);
    else if(TripsState < 0)
      menu_cell_basic_draw(ctx, cell_layer, "error", "", NULL);
    else if(TripsState == STATE_READY && NumTrips == 0)
      menu_cell_basic_draw(ctx, cell_layer, "none found", "", NULL);
    else
      {
        char buffer1[20];
        char buffer2[20];
        trip *tr = &Trips[cell_index->row];
        //APP_LOG(APP_LOG_LEVEL_DEBUG, "row %d",cell_index->row);
        gen_string1(tr->start, tr->duration, buffer1,20);
        gen_string2(tr, buffer2,20);
        menu_cell_basic_draw(ctx, cell_layer,buffer1 ,buffer2, NULL);
      }
    break;
    }

}

static void
window_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);
  // Create the menu layer
  menu_layer = menu_layer_create(bounds);

  // Set all the callbacks for the menu layer
  menu_layer_set_callbacks(menu_layer, NULL, (MenuLayerCallbacks
        )
          { .get_num_sections = menu_get_num_sections_callback, .get_num_rows = menu_get_num_rows_callback, .get_header_height =
              menu_get_header_height_callback, .draw_header = menu_draw_header_callback, .draw_row = menu_draw_row_callback, .select_click =
              menu_select_callback, });

  // Bind the menu layer's click config provider to the window for interactivity
  menu_layer_set_click_config_onto_window(menu_layer, window);

  // Add it to the window for display
  layer_add_child(window_layer, menu_layer_get_layer(menu_layer));

}


static void
window_disappear(Window *window)
{

}

static void
window_appear(Window *window)
{
  CurrentView = VIEW_TRIPS;
  if (TripsState != STATE_READY)
    {
      send_trips_request();
      menu_layer_set_selected_index(menu_layer, (MenuIndex
            )
              { .section = 0, .row = 0 }, MenuRowAlignBottom, false);
      menu_layer_reload_data(menu_layer);
      layer_mark_dirty(menu_layer_get_layer(menu_layer));
    }
}

static void
window_unload(Window *window)
{
  menu_layer_destroy(menu_layer);

}


void
trips_show_window()
{
  window_stack_push(window, true);
}

void
trips_init()
{
  TripsState = STATE_LOADING;

  window = window_create();
  window_set_window_handlers(window, (WindowHandlers
        )
          { .load = window_load, .unload = window_unload, .appear = window_appear, .disappear = window_disappear, });
}

void
trips_deinit()
{
  window_destroy(window);
}
