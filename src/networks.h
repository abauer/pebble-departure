/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

typedef enum Network
{
  NONE = 0,

  // Europe
  RT = 100,

  // Germany
  DB = 200,
  BVG,
  VBB,
  NVV,
  BAYERN,
  MVV,
  INVG,
  AVV,
  VGN,
  VVM,
  VMV,
  HVV,
  SH,
  GVH,
  BSVAG,
  BSAG,
  VBN,
  NASA,
  VVO,
  VMS,
  VGS,
  VRR,
  VRS,
  MVG,
  NPH,
  VRN,
  VRT,
  VVS,
  NALDO,
  DING,
  KVV,
  VAGFR,
  NVBW,
  VVV,

  // Austria
  OEBB = 300,
  VOR,
  WIEN,
  LINZ,
  SVV,
  VVT,
  VMOBIL,
  IVB,
  STV,

  // Switzerland
  SBB = 400,
  BVB,
  VBL,
  ZVV,

  // Belgium
  SNCB = 500,

  // Netherlands
  NS = 600,

  // Denmark
  DSB = 700,

  // Sweden
  SE = 800,
  STOCKHOLM,

  // Norway
  NRI = 900,

  // Luxembourg
  LU = 1000,

  // United Kingdom
  TFL = 1100,
  TLEM,
  TLWM,
  TLSW,

  // Ireland
  TFI = 1200,
  EIREANN,

  // Slovenia
  MARIBOR = 1300,

  // Poland
  PL = 1400,

  // Italy
  ATC = 1500,
  SAD,

  // United Arab Emirates
  DUB = 1600,

  // Israel
  JET = 1700,

  // United States
  SF = 1800,
  SEPTA,

  // Australia
  SYDNEY = 1900,
  MET,

  LAST_NETWORK
} Network;

typedef enum Country
{
  Australia,
  Austria,
  Belgium,
  Denmark,
  Europe,
  Germany,
  Italy,
  Ireland,
  Israel,
  Luxembourg,
  Netherlands,
  Norway,
  Poland,
  UAE,
  UK,
  USA,
  Slovenia,
  Sweden,
  Switzerland,

  LAST_COUNTRY

} Country;

extern Country SortedCountries[];

const char*
getCountryName(Country index);
Network
getNetwork(Country country, int index);
int
getNetworkNumber(Country country);
const char*
getNetworkName(Network network);
