/*
Departure Monitor
Copyright (C) 2014 Andreas Bauer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>

#include <departure.h>


station SavedStations[MAX_STATIONS];
station NearbyStations[MAX_STATIONS];
int8_t NumSavedStations = 0;
int8_t NumNearbyStations = 0;


int8_t JSReady = 0;
int8_t CurrentView = VIEW_NONE;
int Provider = 0;

static void
settings_message_handler(DictionaryIterator *iter, void *context);

static void
reset(int8_t mode);

static void
saveSettings();

static void
loadSettings();

static void
in_received_handler(DictionaryIterator *iter, void *context)
{

  Tuple *msg = dict_find(iter, KEY_JSREADY);
  if (msg)
    {
      JSReady = 1;

      if(CurrentView == VIEW_DEPARTURE)
        {
          send_departure_request();
        }
      else if(CurrentView == VIEW_STATIONS1 || CurrentView == VIEW_STATIONS2 || CurrentView == VIEW_STATIONS3)
        {
          send_stations_request();
        }
      else if(CurrentView == VIEW_TRIPS)
        {
          send_trips_request();
        }
      return;
    }

  msg = dict_find(iter, KEY_STATIONS);
  if (msg)
    {
      stations_message_handler(iter, context);
      return;
    }
    
  msg = dict_find(iter, KEY_SETTINGS);
  if (msg)
    {
      settings_message_handler(iter, context);
      return;
    }

  msg = dict_find(iter, KEY_DEPARTURES);
  if (msg)
    {
      departure_message_handler(iter, context);
      return;
    }

  msg = dict_find(iter, KEY_TRIPS);
  if (msg)
    {
      trips_message_handler(iter, context);
      return;
    }

  msg = dict_find(iter, KEY_TRIP);
  if (msg)
    {
      trip_message_handler(iter, context);
      return;
    }
}

static void
in_dropped_handler(AppMessageResult reason, void *context)
{
  APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Dropped!");
  if (reason == APP_MSG_BUSY)
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Busy");
  if (reason == APP_MSG_BUFFER_OVERFLOW)
    APP_LOG(APP_LOG_LEVEL_DEBUG, "buffer full");
}

static void
out_failed_handler(DictionaryIterator *failed, AppMessageResult reason, void *context)
{
  APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Failed to Send!");

  Tuple *msg = dict_find(failed, KEY_DEPARTURES);
    if (msg)
      {
        LastUpdateDepartures = 0;
      }
}

static void
app_message_init(void)
{
  // Register message handlers
  app_message_register_inbox_received(in_received_handler);
  app_message_register_inbox_dropped(in_dropped_handler);
  app_message_register_outbox_failed(out_failed_handler);
  // Init buffers
  app_message_open(1024, 256);
}

#define PERSIST_DEPARTURESTATE 2
#define PERSIST_DATAVER 4
#define PERSIST_NUMSTATIONS 3
#define PERSIST_STATIONS 0
#define PERSIST_CURRENTSTATION 1
#define PERSIST_PROVIDER 5
#define PERSIST_VIEWMODE 6

static void
settings_message_handler(DictionaryIterator *iter, void *context)
{
  Tuple* key = dict_find(iter, KEY_SETTINGS);
  uint8_t mode = key->value->int32;
  
  if(mode == 1)
  {
    key = dict_find(iter, KEY_PROVIDER);
    int provider = key->value->int32;
    
    key = dict_find(iter, KEY_VIEW);
    ViewMode = key->value->int32;
        
    int8_t m = 0;
    if(provider != Provider)
    {
      m = 1;
    }

    Provider = provider;
    reset(m);
        
    saveSettings();
  }
}

static void
reset(int8_t mode)
{
  departure_reset(mode);
  stations_reset(mode);
  trips_reset(mode);
  trip_reset(mode);
}

static void
loadSettings()
{
  DepartureState = persist_read_int(PERSIST_DEPARTURESTATE);

  NumSavedStations = persist_read_int(PERSIST_NUMSTATIONS);
  persist_read_data(PERSIST_STATIONS, SavedStations, sizeof(station) * NumSavedStations);
  persist_read_data(PERSIST_CURRENTSTATION, &CurrentStation, sizeof(station));

  Provider = persist_read_int(PERSIST_PROVIDER);
  ViewMode = persist_read_int(PERSIST_VIEWMODE);
}

static void
init(void)
{
  loadSettings();

  if (DepartureState > STATE_NONE)
    DepartureState = STATE_LOADING;

  app_message_init();

  stations_init();
  //settings_init();
  departure_init();
  about_init();
  main_view_init();
  trips_init();
  trip_init();

  main_view_show_window();

  if(launch_reason() == APP_LAUNCH_QUICK_LAUNCH && DepartureState > STATE_NONE)
  {
    departure_show_window();
  }
}

static void
saveSettings()
{
  persist_write_int(PERSIST_NUMSTATIONS, NumSavedStations);
  persist_write_int(PERSIST_PROVIDER, Provider);
  persist_write_int(PERSIST_VIEWMODE, ViewMode);
  if (NumSavedStations > 0)
    {
      persist_write_data(PERSIST_STATIONS, SavedStations, sizeof(station) * NumSavedStations);
    }
  persist_write_data(PERSIST_CURRENTSTATION, &CurrentStation, sizeof(station));
  if (DepartureState > STATE_NONE)
    {
      persist_write_int(PERSIST_DEPARTURESTATE, STATE_LOADING);
    }
  else
    {
      persist_write_int(PERSIST_DEPARTURESTATE, STATE_NONE);
    }
  persist_write_int(PERSIST_DATAVER,1);
}

static void
deinit(void)
{
  saveSettings();

  about_deinit();
  stations_deinit();
  //settings_deinit();
  departure_deinit();
  main_view_deinit();
  trips_deinit();
  trip_deinit();
}

int
main(void)
{
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing");

  app_event_loop();
  deinit();
}
